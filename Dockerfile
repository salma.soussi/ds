
FROM openjdk:7-jre-alpine

WORKDIR /app

ARG JAR_FILE=target/myproject-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} app.jar
COPY run.sh .

run chmod  a+X ./run.sh
EXPOSE 8080

ENTRYPOINT ["run.sh"]